let latency = 200;
let id = 0;

function getId(){
  return ++id;
}

let contacts = [
  {
    id:getId(),
    firstName:'John',
    lastName:'Tolkien',
    email:'tolkien@inklings.com',
    phoneNumber:'867-5309'
  },
  {
    id:getId(),
    firstName:'Clive',
    lastName:'Lewis',
    email:'lewis@inklings.com',
    phoneNumber:'867-5309'
  },
  {
    id:getId(),
    firstName:'Owen',
    lastName:'Barfield',
    email:'barfield@inklings.com',
    phoneNumber:'867-5309'
  },
  {
    id:getId(),
    firstName:'Charles',
    lastName:'Williams',
    email:'williams@inklings.com',
    phoneNumber:'867-5309'
  },
  {
    id:getId(),
    firstName:'Roger',
    lastName:'Green',
    email:'green@inklings.com',
    phoneNumber:'867-5309'
  }
];

export class WebAPI {
  isRequesting = false;

  getUsers(id) {
    let serviceUrl = id ? `https://jsonplaceholder.typicode.com/users/${id}` : `https://jsonplaceholder.typicode.com/users`
    console.log(serviceUrl) 
    return fetch(serviceUrl, {
      method: 'get',
      mode: 'cors'
    })
    .then(response => response.json())
    .catch(err => console.log(err))
  }
  
  getContactList(){
    this.isRequesting = true
    
    return new Promise(resolve => {

      this.getUsers()
        .then(users => {
          let results = users.map(user =>  { return {
            id:user.id,
            firstName:user.name.slice(0, user.name.indexOf(" ")),
            lastName:user.name.slice(user.name.indexOf(" "), user.name.length),
            email:user.email,
            phoneNumber: user.phone
          }});
          resolve(results)
          this.isRequesting = false
        })
        .catch(err => {
          console.log(err)
          this.isRequesting = false
        })
    });
  }

  getContactDetails(id){
    this.isRequesting = true;
    return new Promise(resolve => {
      this.getUsers(id)
        .then(user => {
          let responseItem = {
            id:user.id,
            firstName:user.name.slice(0, user.name.indexOf(" ")),
            lastName:user.name.slice(user.name.indexOf(" "), user.name.length),
            email:user.email,
            phoneNumber: user.phone
          }
          resolve(responseItem)
          this.isRequesting = false
        })
        .catch(err => {
          console.log(err)
          this.isRequesting = false
        })
    });
  }

  saveContact(contact){
    this.isRequesting = true;
    return new Promise(resolve => {
      setTimeout(() => {
        let instance = JSON.parse(JSON.stringify(contact));
        let found = contacts.filter(x => x.id == contact.id)[0];

        if(found){
          let index = contacts.indexOf(found);
          contacts[index] = instance;
        }else{
          instance.id = getId();
          contacts.push(instance);
        }

        this.isRequesting = false;
        resolve(instance);
      }, latency);
    });
  }
}
